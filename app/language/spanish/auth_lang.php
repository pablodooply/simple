<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Auth
 * Language: English
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Simple POS - Point of sale made easy
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */

$lang['sign_in']                                    = "Registrarse";
$lang['notify_user_by_email']                       = "Notificar al usuario por correo electronico";
$lang['group']                                      = "Tipo";
$lang['edit_user']                                  = "Editar usuario";
$lang['delete_user']                                = "Eliminar usuario";
$lang['user_added']                                 = "Usuario agregado correctamente ";
$lang['user_updated']                               = "Usuario actualizado correctamente ";
$lang['users_deleted']                              = "Usuarios eliminados correctamente";
$lang['alert_x_user']                               = "Va a eliminar este usuario de forma permanente. Presione Aceptar para continuar y Cancelar para regresar";
$lang['login_email']                                = "Ingrese su Correo";
$lang['edit_profile']                               = "Editar perfil";
$lang['website']                                    = "Pagina web";
$lang['if_you_need_to_rest_password_for_user']      = "Si nececita restablecer la contraseña de este usuario.";
$lang['user_options']                               = "Opcion de Usuario";
$lang['old_password']                               = "Contraseña anterior";
$lang['new_password']                               = "Nueva contraseña";
$lang['change_avatar']                              = "Cambiar avatar";
$lang['update_avatar']                              = "actualizar avatar";
$lang['avatar']                                     = "Avatar";
$lang['avatar_deleted']                             = "Avatar actualizado correctamente";
$lang['captcha_wrong']                              = "Ocurrio un error. Intentelo nuevamente";
$lang['captcha']                                    = "Captcha";
$lang['site_is_offline_plz_try_later']              = "El sitio no esta disponible. Por favor intentelo más tarde.";
$lang['type_captcha']                               = "Ingrese el código";
$lang['we_are_sorry_as_this_sction_is_still_under_development'] = "Lo sentimos, esta pagina sigue en desarrollo. Agradecemos su comprención.";
$lang['confirm']                                    = "Aceptar";
$lang['error_csrf']                                 = "Se detecto una copia de solicitud entre sitios o el token scrf a vencido. Intentelo nuevamente .";
$lang['avatar_updated']                             = "Avatar actualizado correctamente";
$lang['registration_is_disabled']                   = "La cuenta esta cerrada";
$lang['login_to_your_account']                      = "Por favor. Ingrese a su cuenta.";
$lang['pw']                                         = "Contraseña";
$lang['remember_me']                                = "Recordatorio";
$lang['forgot_your_password']                       = "¿Olvidaste tu contraseña?";
$lang['dont_worry']                                 = "No te preocupes!";
$lang['click_here']                                 = "Haga click aquí";
$lang['to_rest']                                    = "Reiniciar";
$lang['forgot_password']                            = "¿Se te olvido tu contraseña?";
$lang['login_successful']                           = "Iniciaste sesión correctamente.";
$lang['back']                                       = "Regresar";
$lang['dont_have_account']                          = "¿No tienes cuenta?";
$lang['no_worry']                                   = "Sin problemas!";
$lang['to_register']                                = "Para Registrarse";
$lang['register_account_heading']                   = "Por favor complete el siguiente formulario para registrar la cuenta";
$lang['register_now']                               = "Registrate ahora";
$lang['no_user_selected']                           = "Ningún usuario seleccionado, seleccion minimo un usuario.";
$lang['delete_users']                               = "Eliminar usuario";
$lang['delete_avatar']                              = "Eliminar Avatar";
$lang['deactivate_heading']                         = "¿Estás seguro de desactivar al usuario?";
$lang['deactivate']                                 = "Desactivado";
$lang['pasword_hint']                               = "Minimo 1 mayúscula, 1 minúscula, 1 número y más de 8 caracteres";
$lang['pw_not_same']                                = "La contraseña y la confirmación no son iguales";
$lang['reset_password']                             = "Restablecer la contraseña";
$lang['reset_password_link_alt']                    = "Puede poner este código en la URL si el enlace anterior no funciona ";
$lang['email_forgotten_password_subject']           = "Restablecer detalles de la contraseña";
$lang['reset_password_email']                       = "Restablecer contraseña %s";
$lang['back_to_login']                              = "Regresa para iniciar sesión";
$lang['forgot_password_unsuccessful']               = "Contraseña restablecida fallida";
$lang['forgot_password_successful']                 = "La contraseña se restableció, utilice la nueva contraseña para iniciar sesión";
$lang['password_change_unsuccessful']               = "Falló el cambio de contraseña";
$lang['password_change_successful']                 = "Contraseña cambiada correctamente";
$lang['forgot_password_email_not_found']            = "El correo electronico ingresado no pertenece a ninguna cuenta.";
$lang['login_unsuccessful']                         = "Acceso fallido, por favor intenta de nuevo";
$lang['email_forgot_password_link']                 = "Restablecer enlace de la contraseña";
$lang['reset_password_heading']                     = "Restablecer la contraseña";
$lang['reset_password_new_password_label']          = "Contraseña nueva";
$lang['reset_password_new_password_confirm_label']  = "Contraseña nueva confirmada";
$lang['register']                                   = "Registrarse";
$lang['reset_password_submit_btn']                  = "Restablezca la contraseña";
$lang['error_csrf']                                 = "Este formulario no pasó por nuestro control de seguridad.";
$lang['account_creation_successful']                = "Cuenta creada correctamente";
$lang['old_password_wrong']                         = "Por favor escriba la contraseña correcta";
$lang['sending_email_failed']                       = "No se puede enviar su correo, si es administrador, vea su configuración; de lo contrario, contacte al administrador.";
$lang['deactivate_successful']                      = "Usuario desactivado correctamente";
$lang['activate_successful']                        = "Usuario activado correctamente";
$lang['login_timeout']                              = "Tienes 3 intentos fallidos, intenta dentro de 10 minutos";
$lang['forgot_password_heading']                    = "Escriba su correo electronico para restablecer su contraseña.";